================
SEZNAM PROJEKTOV
================

**************************
Kako ustvarim nov projekt?
**************************

V podmeniju *Seznam projektov* (v meniju *Vodenje projektov > Seznam projektov*) kliknemo na gumb *Dodaj projekt*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/list.png

V novem oknu vpišemo Ime projekta, kratek *Opis*, nato sistem avtomatično ustvari unikatno Šifro, pod katero je shranjen projekt.
Iz padajočega menija izberemo naročnika projekta.
Če naročnika še nismo vpisali med *Partnerje*, to storimo (glej *Kako dodam novega partnerja, dobavitelja in/ali naročnika?*).
Projektu dodamo *Tehnični opis*, *Opombe tehničnega opisa* in *Opombe materiala* ter kliknemo gumb *Shrani*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/ustvari.png

Projekte je najlažje voditi na pregledu *Časovnice projektov* (*Vodenje projektov > Časovnice projektov*), kjer vas
graf po korakih vodi po zahtevah projekta.

*******************************
Kako uredim podatke o projektu?
*******************************

Projekt uredimo v *Seznamu projektov* (*Vodenje projektov > Seznam projektov*) s klikom na ikono |pencil| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/edit.png

Odpre se okno s podrobnostmi projekta, kjer spremenimo obstoječe podatke.
Ko smo zadovoljni s spremembami, kliknemo gumb *Shrani*.

********************************
Kako označim projekt kot končan?
********************************

Projekt označimo kot končan v Seznamu projektov (*Vodenje projektov > Seznam projektov*) s klikom na ikono |thick| v stolpcu Ukazi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/koncano.png

Projekt lahko zaključimo tudi na pregledu časovnice, tako da zaključimo vse korake in obkljukamo "Označi projekt kot končan".

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/casovnica_koncano.png

***********************
Kako prekličem projekt?
***********************

Projekt prekličemo v *Seznamu projektov* (*Vodenje projektov > Seznam projektov*) s klikom na ikono |icon_cancel_circle| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/preklici.png

***********************************
Kako pogledam podrobnosti projekta?
***********************************

Podrobnosti projekta vidimo, ko v *Seznamu projektov* (*Vodenje projektov > Seznam projektov*) kliknemo na ime projekta (obarvano z modro).

Odpre se nam okno *Podrobnosti projekta*, kjer pregledno vidimo *Ponudbe* na projektu, podrobnosti *Naročila* in *Časovnico projekta*.
Slednja po korakih pokaže, na kateri stopnji realizacije je projekt.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/podrobnosti.png

*******************************************************************
Kako pogledam pričakovano in dejansko porabo materiala na projektu?
*******************************************************************

Podrobnosti projekta vidimo, ko v *Seznamu projektov* (*Vodenje projektov > Seznam projektov*) kliknemo na ime projekta (obarvano z modro).
Odpre se nam okno *Podrobnosti projekta*, kjer v poglavjih *Pričakovana poraba materiala* in *Dejanska poraba materiala*
vidimo pričakovano in dejansko porabo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/poraba.png

*******************************************************
Kako dodam stroške, ki niso definirani kot del procesa?
*******************************************************

Na pregledu *Podrobnosti projekta* (glej *Kako pogledam podrobnosti projekta?*) poiščemo sekcijo *Dodaj stroške/prihodke*,
ki se nahaja pod proračunskim grafom.
Klik na gumb *Dodaj nov strošek/prihodek* na desnem robu vmesnika odpre formo za dodajanje novega stroška ali prihodka,
kjer izpolnemo polja *Datum*, *Količina* in *Opis*.
Novo postavko shranimo s klikom na gumb *Potrdi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/stroski_dodaj.png

*********************************
Kako pregledam proračun projekta?
*********************************
Na pregledu *Podrobnosti projekta* (glej *Kako pogledam podrobnosti projekta?*) poiščemo sekcijo *Proračunski graf*.
Graf kaže predvideno in dejansko gibanje porabe proračuna projekta.
Klik na gumb *Podroben seznam proračuna* nas pelje na pregled s seznamom, ki vsebuje vse postavke porabe.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/poraba_postavke.png

****************************************
Kako rezerviram material za moj projekt?
****************************************

Na pregledu podrobnosti projekta poiščemo poglavje *Rezerviran material*.
Tu so v seznamu podane postavke že rezerviranega materiala.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/rezerviraj_seznam.png

Nove rezervacije urejamo na pregledu *Rezerviraj material za projekt* (s klikom na gumb **Rezerviraj material**).
Vsaka postavka poda pričakovano porabo, porabljeno količino in ne porabljeno rezervirano količino.
Rezervacijo dodamo tako, da v zadnjem stolpcu *Rezerviraj* izpolnemo željeno količino, enoto in skladišče, nato pa na
dnu kliknemo gumb **Rezerviraj**.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/rezerviraj.png

***************************************************
Kako ustvarim ponudbo znotraj Podrobnosti projekta?
***************************************************

Znotraj podrobnosti projekta kliknemo na gumb *Nova ponudba*, ki nam odpre okno *Ustvari ponudbo*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/nova_ponudba.png

V formi znotraj okna so že vpisane podrobnosti projekta, npr. ime, šifra, tehnični opis, itd.
Da ustvarimo ponudbo moramo vpisati v formo manjkajoče informacije, tj. *Dobavne pogoje*, *Plačilne pogoje*, *Datum ponudbe*
(kdaj je bila izdana), *Datum veljavnosti* ponudbe in *Avtorja* teh podatkov.
Da bi hitreje izpolnili formo, Gedaxa že sama izbere *Datum ponudbe* na dan ustvarjanja ponudbe in *Datum veljavnosti*
ponudbe postavi en mesec od datuma izdaje ponudbe.
Datume lahko seveda spreminjate.
Ko končamo kliknemo gumb *Shrani* in ponudba se zabeleži v podrobnosti projekta.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/ponudba.png

Ko je ponudba ustvarjena, nas Gedaxa pelje v novo okno *Posodobitev ponudbe*, kjer nas vpraša ali bomo na ponudbo dodali
*Postavke* ali *Storitve*.

Lahko se odločimo, da postavke dodamo kasneje (glej *Kako dodam postavko na ponudbo*).
Če želimo dodati postavke v tem koraku, kliknemo na gumb *Dodaj postavko*, ki odpre vnosno formo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/ponudba.png

Postavko v formi izberemo iz padajočega menija naših šifrantov.
Če postavke ne vidimo, jo moramo dodati med šifrante (glej *Kako dodam nov šifrant?*).
Vpišemo količino postavke v izbrani merski enoti.
Količina postavke na obdobje pove, koliko te postavke lahko proizvedete v danem časovnem obdobju, za preračunanje časa
izvedbe naročila.
Nazadnje napišemo ceno na enoto postavke, ki bo uporabljena za izračun cene na ponudbi za naročnika.
Podobno lahko dodamo storitev s pritiskom na gumb Dodaj storitev. Na ponudbo lahko dodamo poljubno število postavk in storitev.
Če želimo spremeniti postavke, ki so dodane na ponudbi, jih lahko v istem oknu (*Posodobitev ponudbe*) uredimo s klikom
na |pencil| ali izbrišemo iz ponudbe s klikom na |icon_close| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/ponudba_postavke.png

********************************************************************************************************************
Kako vidim, kateri projekti so odprti, končani ali bili preklicani? Kako sortiram projekte? Kako filtriram projekte?
********************************************************************************************************************

Vse projekte lahko filtriramo na več načinov.
Takoj nad *Seznamom projektov* (*Vodenje projektov > Seznam projektov*) najdemo štiri gumbe *Vsi*, *Odprto*, *Končano*,
*Preklicano*.
Če kliknemo na gumb, nam prikaže tiste projekte, ki odgovarajo filtru gumba, npr. klik na gumb *Končano*, prikaže
seznam projektov, ki so bili uspešno zaključeni.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/seznam_projektov/filter.png

Dodatno lahko projekte sortiramo po vrstnem redu v tabeli *Seznam projektov* s klikom na |icon_updown| za določeno
kategorijo. Na primer klik na |icon_updown| v stolpcu *Šifra*, sortira vse projekte po abecednem vrstnem redu njihove šifre.
Projekte lahko tudi filtriramo s klikom na Filter, ki ga najdemo zgoraj desno nad tabelo.
Odpre nam okno, kjer vpišemo informacije, ki želimo, da se prikažejo v *Seznamu projektov*.
Če na primer vpišemo samo določenega naročnika, bo *Seznam projektov* prikazal vse projekte vezane na tega naročnika a
ne od drugih naročnikov.


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
