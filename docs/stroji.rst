======
STROJI
======

***************
PREGLED STROJEV
***************

Kako vidim zasedenost strojev?
******************************

V opciji *Pregled strojev* (*Glavni meni > Proizvodnja > Stroji > Pregled strojev*) najdemo grafični prikaz zasedenosti strojev po različnih časovnih obdobjih.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/pregled.png

V prvem prikazu vidimo vse stroje v naši proizvodnji (stolpec *Stroji*) ter njihovo zasedenost po izmenah (stolpec *Zasedenost stroja po izmenah [%]*) za izbrani mesec. Zasedenost po izmenah je izračunana kot delež časa, ko je stroj bil uporabljen v izmeni.
Če miškin kursor postavimo nad stolpec v diagramu, se nad stolpcem izpiše, v kateri izmeni je stroj bil zaseden ter delež časa, ko je stroj deloval.

Tak diagram nam omogoča, da hitro vidimo vzorce zasedenosti in neizkoriščenosti in boljše optimiziramo uporabo strojev.

Pod diagrammom zasedenosti strojev za izbran mesec vidimo grafiko, ki prikazuje procentualno, kolikšen del strojev je zaseden v trenutni izmeni.

Če želimo bolj podrobno analizo zasedenosti strojev in zastojev, da boljše optimiziramo njihovo uporabo, gledamo podmeni *Analitika* (*Glavni meni > Proizvodnja > Stroji > Analitika*).


*****
CIKLI
*****

Kaj so cikli (strojev)?
***********************
Cikli strojev so časovna enota (en cikel), v katerem stroj opravi določeno delo. S cikli določimo, koliko časa traje določeno opravilo in kakšni so stroški opravila, da boljše preračunamo tako amortizacijo strojev kot stroške tehničnih operacij.


Kako ustvarimo nov cikel?
*************************

**Pogoji**: da bi ustvarili nov cikel, moramo imeti že vpisane šifrante strojev, šifrante materiala oz. produktov, na katere se cikel nanaša in tehnične operacije, ki bi jih stroj izvedel v ciklu.

V modulu proizvodnja izberemo opcijo *Cikli* (*Glavni meni > Proizvodnja > Stroji > Cikli*). V *Seznamu ciklov* kliknemo na gumb *Dodaj cikel* na dnu tabele.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/dodaj_cikel.png

Odpre se novo okno *Ustvari cikel*, kjer vpišemo podatke.

Iz padajočega menija izberemo *Stroj*, za katerega ustvarjamo cikel. V primeru, da gre za človeško opravilo (npr. čiščenje materiala pred struženjem), pustimo polje *Stroj* prazno in odkljukamo možnost *Človeško opravilo*.
Iz padajočega menija izberemo *Operacijo*, ki jo stroj znotraj cikla opravi. Vpišemo število sekund, ki jih stroj opravi za izvršitev izbrane operacije. Z vnosom *Cene strojne* ure bo možno preračunati dobičkonosnost in amortizacijo glede na cikle in projekte.
V padajočem meniju *Izhod* izberemo končni produkt operacije, ki jo stroj opravli v tem ciklu in *Izhodno količino* končnega produkta cikla ter *Enote* izhodnega produkta.
Če se v ciklu pojavijo še drugi stroški (npr. stroški olja za premične dele), ki niso že upoštevani v strojni uri, jih vpišete v okno *Ostali stroški*. Gedaxa avtomatično sešteje ostale stroške v naslednjem oknu.
S klikom na Potrdi zaključimo ustvarjanje novega cikla.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/ustvari_cikel.png

Do sedaj smo določili tehnične operacije cikla, trajanje, stroške in končni produkt. V naslednjem koraku določimo vhodni material cikla.
Ob potrditvi cikla se v tabeli *Uredi cikel* prikaže gumb *Dodaj vhod* na dnu tabele. Ko pritisnemo gumb, se odpre forma za vpis vhodnega *Materiala*, ki gre v cikel, njegove *Količine* merjene v izbrani *Enoti* in Ostanek materiala po zaključku cikla.
Pozor: ostanek je merjen v procentih! Primer: za cikel pranja je vhodni material 4m Alumijastih palic, kjer na koncu pranja ostanejo 100% materiala. Če bi bil cikel brušenje 4kg Aluminijastih palic, bi ostanek bil na koncu lahko 98% Aluminijastih palic, ker je 2% materiala odpadlo v opilkih brušenja.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/ustvari_vhod.png

Zakaj najprej definiramo končni produkt cikla in šele nato naslednji? Vsak cikel ima samo en končni produkt. Lahko pa ima več vhodnih, kot se na primer zgodi pri varjenju ali lepljenju. Posledično je cikel lažje edinstveno definirati skozi izhodni - končni - material.

***********
VZDRŽEVANJE
***********

Kako planiramo vzdrževanje strojev?
***********************************

V podmeniju *Stroji* izberemo opcijo *Vzdrževanje* (*Glavni meni > Proizvodnja > Stroji > Vzdrževanje*). Prikaže se tabela z vsemi stroji, ki smo jih vpisali pod šifrante. S klikom na stroj, se odpre podroben plan vzdrževanja.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/vzdrzevanje.png

Novo vzdrževanje vpišemo s klikom na gumb *Dodaj vzdrževanje*, kjer v formo vpišemo poljubni *Opis vzdrževanja* (npr. tehnični pregled, čiščenje, menjava delov, itd.) in kdaj bo vzdrževanje potekalo (*Datum*). Če želimo lahko dodamo možnost, da se vzdrževanje ponavlja, tako da določimo kako pogosto se to zgodi (*Ponovi*)  znotraj nekega *Intervala* ponovitve (npr. v dnevih, tednih, mesecih ali letih). Če želimo, lahko dodamo opombe na vzdrževanje, npr. kontakt tehnika, ki bo vodil vrdrževanje.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/ustvari_vzdrzevanje.png

Ko kliknemo na gumb *Potrdi*, se novo vzdrževanje vpiše v tabelo *Pregled stroja* in na koledar na *Nadzorni plošči*.

Vzdrževanje lahko urejamo s pritiskom na ikono |pencil|. S pritiskom na |thick| potrdimo, da je bilo vzdrževanje opravljeno. Če smo vnesli interval ponavljanja, se bo datum naslednjega vzdrževanja avtomatično posodobil na naslednji datum vzdrževanja. Če ponavljanja nismo vnesli, se bo po potrditvi vzdrževanje zbrisalo iz tabele.

Če želimo, lahko dodamo datoteke vezane na vzdrževanje (gumb *Dodaj datoteko*). Lahko dodamo različne datoteke, npr. pogodbe z vzdrževalnimi servisi, slike dela stroja, ki je potreben vzdrževanja, itd.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/vzdrzevanje_datoteka.png

V zavihku opombe lahko vpišemo poljubne informacije, ki jih želimo dodati glede vzdrževanja stroja.

Kje vidimo vsa vzdrževanja strojev, ki so bila opravljena?
**********************************************************

V *Dnevniku vzdrževanja* so napisana vsa planirana vzdrževanja (stolpec *Opis*), na kateri datum so bila načrtovana vzdrževalna dela (stolpec *Planiran zaključek*), kdaj je bilo interno potrjeno, da je vzdrževanje zaključeno (stolpec *Datum zaključka*) in kdo je potrdil, da je vzdrževanje bilo opravljeno (stolpec *Uporabnik*).

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/vzdrzevanje_plan.png

S klikom na konkretno vzdrževanje se odpre nov zavihek s pregledom podrobnosti o vzdrževanju, ki smo jih vpisali v podmeniju Planirano vzdrževanje.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/vzdrzevanje_podrobnosti.png

*********
ANALITIKA
*********

Kaj nam pove *Analitika* strojev?
*********************************

V podmeniju *Analitika* (*Glavni meni > Proizvodnja > Stroji > Analitika*) dobimo vpogled v uporabo strojev in njihove zastoje.

.. image:: .. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/stroji/analitika.png

Na vrhu strani izberemo obdobje med dvema datuma. Spodaj se avtomatično izrišejo grafi uporabe strojev v izbranem obdobju, za vsak stroj posamično.

Grafi izrišejo odstotek časa, ko je stroj bil uporabljen v vseh izmenah za izbrani datum. Pod vsakim grafom vidimo podatke o zastojih, datum, kdaj se je zastoj zgodil, trajanje zastoja in morebitne opombe. Če zastojev nismo zabeležili, ostane tabela pod grafom prazna.


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
